import mongoose, { connection } from 'mongoose'

module.exports.connection = async () => {
  try {
    await mongoose.connect(process.env.MONGO_DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    console.log(`DB is now connected! `)
  } catch (error) {
    console.error(`There was an error: ${error}`)
  }
}

module.exports.isValidObjectId = (id) => {
  return mongoose.Types.ObjectId.isValid(id)
}
