import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import User from '../../database/models/user'
import Task from '../../database/models/task'
import { combineResolvers } from 'graphql-resolvers'
import { isAuthenticated } from './middleware'
import PubSub from '../../subscription'
import { userEvents } from '../../subscription/events'

module.exports = {
  Query: {
    user: combineResolvers(isAuthenticated, async (_, __, { email }) => {
      try {
        const user = await User.findOne({ email })
        console.log(email)
        if (!user) {
          throw new Error('User not found!')
        }
        return user
      } catch (error) {
        console.error(error)
        throw error
      }
    }),
  },
  Mutation: {
    signup: async (_, { input }) => {
      try {
        const user = await User.findOne({ email: input.email })
        if (user) {
          throw new Error('Email already in use')
        }
        const hashedPassword = await bcrypt.hash(input.password, 12)
        const newUser = new User({ ...input, password: hashedPassword })
        const result = await newUser.save()
        PubSub.publish(userEvents.USER_CREATED, {
          userCreated: result,
        })
        return result
      } catch (error) {
        console.error(`There was an error: ${error}`)
        throw error
      }
    },
    login: async (_, { input }) => {
      try {
        const user = await User.findOne({ email: input.email })
        if (!user) {
          throw new Error('User not found')
        }
        const isPasswordValid = await bcrypt.compare(
          input.password,
          user.password
        )
        if (!isPasswordValid) {
          throw new Error('Incorrect Password')
        }
        const key = process.env.JWT_SECRET_KEY || 'defkeyapolloapi'
        const token = jwt.sign({ email: user.email }, key, { expiresIn: '6d' })
        return { token }
      } catch (error) {
        console.error(`There was an error: ${error}`)
        throw error
      }
    },
  },
  Subscription: {
    userCreated: {
      subscribe: () => PubSub.asyncIterator(userEvents.USER_CREATED),
    },
  },
  User: {
    tasks: async ({ id }) => {
      try {
        const tasks = await Task.find({ user: id })
        return tasks
      } catch (error) {
        console.error(error)
        throw error
      }
    },
  },
}
