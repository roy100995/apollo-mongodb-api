import { skip } from 'graphql-resolvers'
import Task from '../../../database/models/task'
import { isValidObjectId } from '../../../database/util'

module.exports.isAuthenticated = (_, __, { email }) => {
  if (!email) {
    throw new Error('Access denied!. Please login before continue')
  }
  return skip
}

module.exports.isTaskOwner = async (_, { id }, { loggedInUserId }) => {
  try {
    if (!isValidObjectId(id)) {
      throw new Error('Invalid id')
    }
    const task = await Task.findById(id)
    if (!task) {
      throw new Error('Task not found')
    } else if (task.user.toString() !== loggedInUserId) {
      throw new Error('Not authorized as task owener')
    }
    return skip
  } catch (error) {
    console.error(error)
    throw error
  }
}
