import { gql } from 'apollo-server-express'

export const typeDefs = gql`
  scalar Date

  type Query {
    tasks(cursor: String, limit: Int): TaskFeed
    task(id: ID!): Task
    user: User
  }

  type Mutation {
    createTask(input: createTaskInput): Task
    updateTask(id: ID!, input: UpdateTaskInput!): Task
    deleteTask(id: ID!): Task
    signup(input: SignupInput): User
    login(input: LoginInput): Token
  }

  input SignupInput {
    name: String!
    email: String!
    password: String!
  }

  input LoginInput {
    email: String!
    password: String!
  }

  input createTaskInput {
    name: String!
    completed: Boolean!
  }

  input UpdateTaskInput {
    name: String
    completed: Boolean
  }

  type User {
    id: ID!
    name: String!
    email: String!
    tasks: [Task!]
    createdAt: Date!
    updatedAt: Date!
  }

  type Task {
    id: ID!
    name: String!
    completed: Boolean!
    user: User!
    createdAt: Date!
    updatedAt: Date!
  }

  type TaskFeed {
    taskFeed: [Task!]
    pageInfo: PageInfo!
  }

  type PageInfo {
    nextPageCursor: String
    hasNextPage: Boolean
  }

  type Token {
    token: String!
  }

  type Subscription {
    userCreated: User
  }
`
