import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import cors from 'cors'
import dotEnv from 'dotenv'
import resolvers from './resolvers'
import { typeDefs } from './schema'
import { connection } from '../database/util'
import { verifyUser } from '../helper/context'
import Dataloader from 'dataloader'
import loaders from '../loaders'

dotEnv.config()

const app = express()
connection()
const PORT = process.env.PORT || 3000

const apolloServer = new ApolloServer({
  typeDefs: typeDefs,
  resolvers: resolvers,
  context: async ({ req, connection }) => {
    const contextObj = {}
    if (req) {
      await verifyUser(req)
      ;(contextObj.email = req.email),
        (contextObj.loggedInUserId = req.loggedInUserId)
    }
    contextObj.loaders = {
      user: new Dataloader((keys) => loaders.user.batchUsers(keys)),
    }
    return contextObj
  },
  formatError: (error) => {
    console.log(error)
    return {
      message: error.message,
    }
  },
})

apolloServer.applyMiddleware({ app, path: '/graphql' })

app.use(cors())
app.use(express.json())
app.use('/', (req, res, next) => {
  res.send({ message: 'Hello World' })
})

const httpServer = app.listen(PORT, () => {
  console.log(`Server is runnning on port ${PORT}`)
  console.log(`Graphql Endpoint: ${apolloServer.graphqlPath}`)
})

apolloServer.installSubscriptionHandlers(httpServer)
