import User from '../database/models/user'

module.exports.batchUsers = async (usersId) => {
  console.log(`keys -> ${usersId}`)
  const users = await User.find({ _id: { $in: usersId } })
  return usersId.map((userId) => users.find((user) => user.id === userId))
}
